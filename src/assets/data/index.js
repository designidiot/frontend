
export const NAVS = [
  {
    url: '/',
    name: 'Work',
    large: true,
  },
  {
    url: '/solutions',
    name: 'Solutions',
    large: true,
  },
  {
    url: '/services',
    name: 'services',
    large: true,
  },
  {
    url: '/about-us',
    name: 'About us',
    large: true,
  },
  {
    url: '/blog',
    name: 'Blog',
    large: true,
  },
  {
    url: null,
    name: 'Request a quote',
    large: true,
  },
  {
    url: null,
    name: 'Let\'s chat',
    large: false,
  },
]

export const SOCIALS = [
  {
    link: 'https://www.facebook.com/codigo.co/',
    icon: 'https://www.codigo.co/img/icons/social-facebook.svg',
    iconWhite: 'https://www.codigo.co/img/icons/social-facebook-white.svg',
    width: 12,
    height: 22,
  },
  {
    link: 'https://twitter.com/CodigoApps',
    icon: 'https://www.codigo.co/img/icons/social-twitter.svg',
    iconWhite: 'https://www.codigo.co/img/icons/social-twitter-white.svg',
    width: 21,
    height: 19,
  },
  {
    link: 'https://www.instagram.com/hellocodigo/',
    icon: 'https://www.codigo.co/img/icons/social-instagram.svg',
    iconWhite: 'https://www.codigo.co/img/icons/social-instagram-white.svg',
    width: 21,
    height: 19,
  },
  {
    link: 'https://www.linkedin.com/company/codigo-pte-ltd',
    icon: 'https://www.codigo.co/img/icons/social-linkedIn.svg',
    iconWhite: 'https://www.codigo.co/img/icons/social-linkedIn-white.svg',
    width: 20,
    height: 20,
  },
]

export const CATEGORIES = [
  {
    id: 0,
    name: 'All',
    tags: 'all'
  },
  {
    id: 1,
    name: 'Food & Beverage',
    tags: 'food-and-beverage'
  },
  {
    id: 2,
    name: 'Media',
    tags: 'media'
  },
  {
    id: 3,
    name: 'Transport & Logistics',
    tags: 'transport-and-logistics'
  },
  {
    id: 4,
    name: 'Banking & Finance',
    tags: 'banking-finance'
  },
  {
    id: 5,
    name: 'Lifestyle',
    tags: 'lifestyle'
  },
  {
    id: 6,
    name: 'Co-incubation',
    tags: 'co-incubation'
  },
  {
    id: 7,
    name: 'Healthcare',
    tags: 'healthcare'
  },
  {
    id: 8,
    name: 'Retail & Entertainment',
    tags: 'retail-and-entertainment'
  },
  {
    id: 9,
    name: 'Telco',
    tags: 'telco'
  },
  {
    id: 10,
    name: 'Others',
    tags: 'others'
  },
  {
    id: 11,
    name: 'Start-ups',
    tags: 'start-ups'
  },
]

export const WORKS = [
  {
    id: 1,
    img: 'https://cdn.codigo.co/uploads/2018/04/711.jpg',
    category: 'retail-and-entertainment',
    name: '7Rewards',
    legends: ['app', 'web', 'cms'],
    theme: 'light',
  },
  {
    id: 2,
    img: 'https://cdn.codigo.co/uploads/2018/12/MYR.jpg',
    category: 'telco',
    name: 'MyRepublic Mobile',
    legends: ['app', 'uiux'],
    theme: 'light',
  },
  {
    id: 3,
    img: 'https://cdn.codigo.co/uploads/2018/08/CDG-2-1.jpg',
    category: 'transport-and-logistics',
    name: 'ComfortDelGro',
    legends: ['app', 'uiux'],
    theme: 'dark',
  },
  {
    id: 4,
    img: 'https://cdn.codigo.co/uploads/2018/08/FPOL.jpg',
    category: 'retail-and-entertainment',
    name: 'FairPrice Online',
    legends: ['uiux'],
    theme: 'light',
  },
  {
    id: 5,
    img: 'https://cdn.codigo.co/uploads/2018/12/ck.jpg',
    category: 'retail-and-entertainment',
    name: 'Charles & Keith / Pedro',
    legends: ['app'],
    theme: 'dark',
  },
  {
    id: 6,
    img: 'https://cdn.codigo.co/uploads/2018/11/thumbnail-justice_league@2x.jpg',
    category: 'retail-and-entertainment',
    name: 'Justice League',
    legends: ['app'],
    theme: 'light',
  },
  {
    id: 7,
    img: 'https://cdn.codigo.co/uploads/2018/12/FF-1.jpg',
    category: 'transport-and-logistics,co-incubation,start-ups',
    name: 'FastFast',
    legends: ['app', 'web', 'cms'],
    theme: 'light',
  },
  {
    id: 8,
    img: 'https://cdn.codigo.co/uploads/2018/11/fullerton-health-thumbnail@2x.jpg',
    category: 'healthcare',
    name: 'Fullerton Health',
    legends: ['app'],
    theme: 'light',
  },
  {
    id: 9,
    img: 'https://cdn.codigo.co/uploads/2018/11/skypremium-thumbnail@2x.jpg',
    category: 'lifestyle',
    name: 'Sky Premium',
    legends: ['app', 'web'],
    theme: 'light',
  },
  {
    id: 10,
    img: 'https://cdn.codigo.co/uploads/2018/12/TSingapore-thumbnail@2x.jpg',
    category: 'lifestyle',
    name: 'T Singapore',
    legends: ['app', 'cms'],
    theme: 'light',
  },
  {
    id: 11,
    img: 'https://cdn.codigo.co/uploads/2018/12/singpost-thumbnail@2x.jpg',
    category: 'transport-and-logistics',
    name: 'SingPost',
    legends: ['app', 'uiux'],
    theme: 'light',
  },
  {
    id: 12,
    img: 'https://cdn.codigo.co/uploads/2018/12/dineinn.jpg',
    category: 'co-incubation,start-ups,food-and-beverage',
    name: 'Dine Inn',
    legends: ['app', 'web', 'cms'],
    theme: 'light',
  },
  {
    id: 13,
    img: 'https://cdn.codigo.co/uploads/2018/11/siemens.jpg',
    category: 'transport-and-logistics',
    name: 'Siemens Engineering',
    legends: ['app', 'web', 'cms'],
    theme: 'light',
  },
  {
    id: 14,
    img: 'https://cdn.codigo.co/uploads/2018/12/f1.jpg',
    category: 'others',
    name: 'Singapore Grand Prix',
    legends: ['app', 'web', 'cms'],
    theme: 'light',
  },
  {
    id: 15,
    img: 'https://cdn.codigo.co/uploads/2018/12/mc-cafe-thumbnail@2x.jpg',
    category: 'food-and-beverage',
    name: 'McDonald\'s',
    legends: ['app'],
    theme: 'light',
  },
]
