import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import AppHeader from '@/components/header/AppHeader'
import AppFooter from '@/components/footer/AppFooter'
import Welcome from '@/components/welcome/Welcome'

Vue.config.productionTip = false

Vue.component('AppHeader', AppHeader)
Vue.component('AppFooter', AppFooter)
Vue.component('Welcome', Welcome)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
