import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import {
  CATEGORIES,
  WORKS
} from '@/assets/data'

export default new Vuex.Store({
  state: {
    init: true,
    delay: 2000,
    categories: CATEGORIES,
    works: WORKS,
  },
  mutations: {
    setInit(state, init) {
      state.init = init
    }
  },
  getters: {
    getWorks: (state) => (tag) => {
      let works = []
      if (tag === 'all') {
        works = state.works.map(work => ({
          ...work,
          category: getCategories(work.category, state.categories)
        }))
      } else {
        works = state.works.filter(work => isCategory(work.category, tag)).map(work => ({
          ...work,
          category: getCategories(work.category, state.categories)
        }))
      }
      return works
    },
  },
  actions: {
  },
  modules: {
  }
})

function getCategories(tags, categories) {
  const arr = tags.split(',')
  return categories
    .filter(cate => arr.find(a => a === cate.tags))
    .map(cate => cate.name)
    .join(', ')
}

function isCategory(tags, tag) {
  const arr = tags.split(',')
  return arr.find(a => a === tag)

}
