# Codigo test
Please see the [live demo here](https://codigo-test-2020.web.app/)

## Fixed errors on current website

### 1. Navigation menu is unusable at **Small Device** or **Mobile landscape**
![Error one](./src/assets/IMG_0136.PNG)

### 2. Filter is unusable at **Small Device** or **Mobile landscape**
![Error two](./src/assets/IMG_0135.PNG)
![Error two landscape](./src/assets/IMG_0137.PNG)

### 3. Page scroll is not smooth on **iOS, iPadOS saferi**

### Vue install
```
npm install -g vue-cli
```

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
